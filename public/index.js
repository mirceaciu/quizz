document.addEventListener("DOMContentLoaded", function(event) {
    let index = 0;

    const main = document.querySelector('.main');
    const audio = new Audio(`./resources/audio/${fdata.intro.soundTrigger}`);
    
    window.audio = audio;


    const questionContainer = document.querySelector('.question');
    const answersContainer = document.querySelector('.answers');

    const answerTemplate = document.querySelector('#answer-template');
    let human = {};

    const advance = () => {
        index += 1;
        window.audio.pause();
        
        askHumanDataFromNative();
        
        if(index <= 4 ){
            questionContainer.innerHTML = fdata.questions[index].title;
            answersContainer.innerHTML = '';
            main.style.backgroundImage = `url('./resources/images/background/${ fdata.questions[index].background}')`

            window.audio.src = `./resources/audio/${fdata.questions[index].soundTrigger}`;
            window.audio.play();

            const answers = fdata.questions[index].answers;
            for (const answer_i in answers) {
                
                const ansClone = answerTemplate.content.cloneNode(true);
                const ansCloneLabel = ansClone.querySelector(".label");
                ansCloneLabel.innerHTML = answers[answer_i].label;
                
                ansCloneLabel.parentElement.onclick = advance;
                answersContainer.appendChild(ansClone);
            }
        }else{
            main.style.backgroundImage = `url('./resources/images/background/${ fdata.outro.background}')`
            questionContainer.innerHTML = fdata.outro.title;
    
            window.audio.src = `./resources/audio/${fdata.outro.soundTrigger}`;
            window.audio.play();

            document.querySelector('.answers').style.display = 'none'
        }

    }

    const askHumanDataFromNative = () => {
        window.ReactNativeWebView && window.ReactNativeWebView.postMessage("getHumanDataFromNative");
    }

    document.addEventListener("message", message => {
        human = JSON.parse(message.data);
        document.getElementById('tooltip').textContent = human.gender ? `${human.gender} | ${human.emotion} | ${human.age}` : 'no-one';
    });

    // show intro
    questionContainer.innerHTML = fdata.intro.title;
    
    window.audio.play();

    const ansClone = answerTemplate.content.cloneNode(true);
    const ansCloneLabel = ansClone.querySelector(".label");
    ansCloneLabel.textContent = fdata.intro.cta;

    answersContainer.appendChild(ansClone);

    ansCloneLabel.parentElement.onclick = advance;
})
