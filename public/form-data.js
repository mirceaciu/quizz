const fdata = {
    intro : {
        soundTrigger: 'intro.wav',
        title: 'Hai să vedem <br/>ce bere<br/>ți se potrivește',
        cta: 'Află ce ți-am pregătit!',
        background: 'bg2.jpg'
    },
    questions: {
        1: {
            soundTrigger: '1.wav',
            title: 'Când vine pofta de bere fără alcool?',
            background: 'bg3.jpg',
            answers: [
                {label: 'Seara după sală', icon: 'ball', class: 'ball'},
                {label: 'La birou', icon: 'laptop', class: 'laptop'},
                {label: 'La masa de prânz', icon: 'eat', class: 'eat'},
                {label: 'În timpul liber', icon: 'car', class: 'car'},
            ]
        },
        2: {
            soundTrigger: '2.wav',
            title: 'Ce te prinde la berea fără alcool',
            background: 'bg4.jpg',
            answers: [
                {label: 'Aroma fină<br/>și gustul delicat'},
                {label: 'Pot bea cu prietenii<br/>tot timpul'},
                {label: 'O beau oriunde<br/>și oricând'},
                {label: 'Mix-ul de bere cu suc'},
            ]
        },
        3: {
            soundTrigger: '3.wav',
            title: 'Câte intră?',
            background: 'bg5.jpg',
            answers: [
                {label: 'Una de poftă'},
                {label: 'Două maxim'},
                {label: 'Trei de sete'},
                {label: 'Patru<br/>să am suficient la mine'},
            ]
        },
        4: {
            soundTrigger: '4.wav',
            title: 'Cum ești după ce o savurezi?',
            background: 'bg6.jpg',
            answers: [
                {label: 'Răcorit'},
                {label: 'În formă'},
                {label: 'Mai vesel'},
                {label: 'Energizat'},
            ]
        },
    },
    outro : {
        soundTrigger: 'outro.wav',
        title: 
            'Când ești tânără și iți place să fii mereu în formă, un mix de bere cu suc, de poftă, ' +
            'îți va da energia de care ai nevoie. Ia-ți acum Coolerul preferat!',
        background: 'bg7.jpg'
    },

}